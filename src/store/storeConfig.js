import { createStore, combineReducers } from 'redux' //importando redux
//no index js, importo o provider para a relacao entre o redux e react.

import numerosActions from './reducers/numerosActions'

const reducers = combineReducers({
    //criei um reducer
    numeros: numerosActions,
})

//funcao que gerencia o estado a partir do reducer

function storeConfig() {
    return createStore(reducers)
}

export default storeConfig