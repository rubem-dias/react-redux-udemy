import React from 'react'
import Card from '../Card/Card.jsx'

import { connect } from 'react-redux'


function Soma(props) {

    const minNumero = props.min
    const maxNumero = props.max

    return (
        <Card title="Soma dos Numeros" blue>
            <div>
            <span>
                <span> Resultado: </span>
                <strong> {(minNumero+ maxNumero)} </strong>
            </span>
            </div>
        </Card>
    )
}

function mapStateToProps(state) {
    return {
        min: state.numeros.min,
        max: state.numeros.max
    }
}

export default connect(mapStateToProps)(Soma)