
import React from 'react'
import { connect } from 'react-redux' //conecta o componente e o estado da aplicacao


import Card from '../Card/Card.jsx'

const Media = props => {

    return (
        <Card title="Media dos Numeros" green>
            <div>
            <span>
                <span> Resultado: </span>
                <strong> {(props.numMax + props.numMin) / 2} </strong>
            </span>
            </div>
        </Card>
    )
}

function mapStateToProps(state) {
    return {
        numMin: state.numeros.min,
        numMax: state.numeros.max,
    }
}
  
export default connect(mapStateToProps)(Media)