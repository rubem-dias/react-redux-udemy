import './Intervalo.css'
import React from 'react'
import Card from '../Card/Card.jsx'
import { connect } from 'react-redux'
import { alterarMin, alterarMax } from '../../store/actions/numeros'

function Intervalo(props) {

    const { min, max } = props

    return (
        <Card title="Intervalo de Numeros" red>
            <div className="Intervalo">
            <span>
                <strong> Minimo: </strong>
                <input type="number" value={min}
                    onChange={e => props.alteraOMinimo(+e.target.value)}
                />
            </span>
            <span>
                <strong> Maximo: </strong>
                <input type="number" value={max}
                    onChange={e => props.alteraOMaximo(+e.target.value)}
                />
            </span>
            </div>
        </Card>
    )
}

function mapStateToProps(state) {
    return {
        min: state.numeros.min,
        max: state.numeros.max
    }
}

function mapDispatchToProps(dispatch) {
    return {
        alteraOMinimo(novoNumero) {
            //action creator retorna uma action
            const action = alterarMin(novoNumero)
            dispatch(action)
        },
        alteraOMaximo(novoNumero) {
            const action = alterarMax(novoNumero)
            dispatch(action)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Intervalo)